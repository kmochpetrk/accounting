`mvn clean install`
`mvn fabric8:deploy`

###### Dalsi prikazy
``minishift config set vm-driver virtualbox``
``oc new-project boot``
``@FOR /f "tokens=* delims=^L" %i IN ('minishift docker-env') DO @call %i``
``minishift start --vm-driver virtualbox``

###### link
`http://accounting-boot.192.168.99.100.nip.io/`
`http://accounting-boot.192.168.99.100.nip.io`