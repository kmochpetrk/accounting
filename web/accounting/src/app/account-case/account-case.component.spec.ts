import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountCaseComponent } from './account-case.component';

describe('AccountCaseComponent', () => {
  let component: AccountCaseComponent;
  let fixture: ComponentFixture<AccountCaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
