import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {AccountsService} from "../accounts.service";
import {AccountCasesResponse, DebitCreditRow, MainBookRow} from "../select-accounts-row/cases-types";
import {SelectAccountsRowComponent} from "../select-accounts-row/select-accounts-row.component";

@Component({
  selector: 'app-account-case',
  templateUrl: './account-case.component.html',
  styleUrls: ['./account-case.component.css']
})
export class AccountCaseComponent implements OnInit {

  mainBookRows: DebitCreditRow[] = [];

  accountCasesResponse: AccountCasesResponse;

  @ViewChildren(SelectAccountsRowComponent) rows: QueryList<SelectAccountsRowComponent>;

  constructor(private accountService: AccountsService) { }

  ngOnInit() {
    this.accountService.readAccountCases().subscribe(resp => {
      this.accountCasesResponse = resp;
      const mainBookRowsDebit = resp.content[0].mainBookRows.filter(one => one.mbWriteType === 'DEBIT');
      const mainBookRowsCredit = resp.content[0].mainBookRows.filter(one => one.mbWriteType === 'CREDIT');
      const max = Math.max(mainBookRowsCredit.length, mainBookRowsDebit.length);
      for (let i = 0 ; i < max; i++) {
        const debitCreditRow = new DebitCreditRow();
        if (i < mainBookRowsDebit.length) {
          debitCreditRow.debit = mainBookRowsDebit[i];
        } else {
          debitCreditRow.debit = new MainBookRow();
          debitCreditRow.debit.mbWriteType = 'DEBIT';
        }
        if (i < mainBookRowsCredit.length) {
          debitCreditRow.credit = mainBookRowsCredit[i];
        } else {
          debitCreditRow.credit = new MainBookRow();
          debitCreditRow.credit.mbWriteType = 'CREDIT';
        }
        this.mainBookRows.push(debitCreditRow);
      }

    })


  }

  addRow() {
    const debitCreditRow = new DebitCreditRow();
    debitCreditRow.credit = new MainBookRow();
    debitCreditRow.credit.mbWriteType = 'CREDIT';
    debitCreditRow.debit = new MainBookRow()
    debitCreditRow.debit.mbWriteType = 'DEBIT';
    this.mainBookRows.push(debitCreditRow);
  }

  saveAll() {
    const model = []
    this.rows.forEach(row => {
      row.saveModel().forEach(one => {
        model.push(one);
      });
    });
    this.accountCasesResponse.content[0].mainBookRows = model;
    // this.mainBookRows.forEach((dcRow) => {
    //   this.accountCasesResponse.content[0].mainBookRows.push(dcRow.debit);
    //   this.accountCasesResponse.content[0].mainBookRows.push(dcRow.credit);
    // })
    this.accountService.updateAccountCase(this.accountCasesResponse.content[0].id, this.accountCasesResponse.content[0]).subscribe(resp =>{
      console.log('RESP!!!');
      console.log(JSON.stringify(resp));
    })
  }
}
