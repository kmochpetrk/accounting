import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { GlAccountSelectComponent } from './gl-account-select/gl-account-select.component';
import {MatButtonModule, MatCardModule, MatIconModule, MatInputModule} from "@angular/material";
import {MatSelectModule} from '@angular/material/select';
import {AccountsService} from "./accounts.service";
import {HttpClientModule} from "@angular/common/http";
import { SelectAccountsRowComponent } from './select-accounts-row/select-accounts-row.component';
import { AccountCaseComponent } from './account-case/account-case.component';

@NgModule({
  declarations: [
    AppComponent,
    GlAccountSelectComponent,
    SelectAccountsRowComponent,
    AccountCaseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    MatCardModule,
    MatSelectModule,
    HttpClientModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule
  ],
  providers: [AccountsService],
  bootstrap: [AccountCaseComponent]
})
export class AppModule { }
