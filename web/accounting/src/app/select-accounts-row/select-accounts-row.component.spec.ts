import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectAccountsRowComponent } from './select-accounts-row.component';

describe('SelectAccountsRowComponent', () => {
  let component: SelectAccountsRowComponent;
  let fixture: ComponentFixture<SelectAccountsRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectAccountsRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectAccountsRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
