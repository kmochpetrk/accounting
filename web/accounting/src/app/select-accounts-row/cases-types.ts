import {Glaccount} from "../gl-account-select/types";



  // export interface GlAccount {
  //   id: number;
  //   acctNo: string;
  //   description: string;
  //   glAccountType: string;
  // }

  export enum DEBIT_CREDIT {
    DEBIT, DREDIT
  }


  export class MainBookRow {
    id: number;
    amount: number;
    mbWriteType: string;
    glAccount: Glaccount;
    mainBookCase?: any;
  }

  export interface Content {
    id: number;
    mainBookCaseType: string;
    description: string;
    mainBookRows: MainBookRow[];
    variableSymbol?: string;
    constantSymbol?: string;
  }

  export interface Sort {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
  }

  export interface Pageable {
    sort: Sort;
    offset: number;
    pageSize: number;
    pageNumber: number;
    paged: boolean;
    unpaged: boolean;
  }

  export interface Sort2 {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
  }

  export class AccountCasesResponse {
    content: Content[];
    pageable: Pageable;
    totalElements: number;
    totalPages: number;
    last: boolean;
    number: number;
    size: number;
    sort: Sort2;
    numberOfElements: number;
    first: boolean;
    empty: boolean;
  }

  export class DebitCreditRow {
    debit: MainBookRow;
    credit: MainBookRow;
  }


