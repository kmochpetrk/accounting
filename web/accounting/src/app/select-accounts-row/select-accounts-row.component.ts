///<reference path="../gl-account-select/gl-account-select.component.ts"/>
import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {GlAccountSelectComponent} from "../gl-account-select/gl-account-select.component";
import {DebitCreditRow, MainBookRow} from "./cases-types";

@Component({
  selector: 'app-select-accounts-row',
  templateUrl: './select-accounts-row.component.html',
  styleUrls: ['./select-accounts-row.component.css']
})
export class SelectAccountsRowComponent implements OnInit {

  @Input()
  debitCreditRow: DebitCreditRow;


  @ViewChild('debit', {static: false})
  glAccountSelectComponentDebit: GlAccountSelectComponent;

  @ViewChild('credit', {static: false})
  glAccountSelectComponentCredit: GlAccountSelectComponent;

  constructor() {
  }

  ngOnInit() {
  }

  public saveModel(): MainBookRow[] {

    console.log('Model Debit: ' + JSON.stringify(this.glAccountSelectComponentDebit.getSelectedAccountWithAmount()));
    console.log('Model Credit: ' + JSON.stringify(this.glAccountSelectComponentCredit.getSelectedAccountWithAmount()));

    const credit = this.glAccountSelectComponentCredit.getSelectedAccountWithAmount();
    const debit = this.glAccountSelectComponentDebit.getSelectedAccountWithAmount();

    this.debitCreditRow.credit.amount = credit.amount;
    this.debitCreditRow.credit.glAccount = credit.account;

    this.debitCreditRow.debit.amount = debit.amount;
    this.debitCreditRow.debit.glAccount = debit.account;

    return [this.debitCreditRow.debit, this.debitCreditRow.credit];
  }

}
