import {ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {AccountsService} from "../accounts.service";
import {CreateAccountAmount, Glaccount, GlAccount} from "./types";

@Component({
  selector: 'app-gl-account-select',
  templateUrl: './gl-account-select.component.html',
  styleUrls: ['./gl-account-select.component.css']
})
export class GlAccountSelectComponent implements OnInit, OnChanges {

  accounts: Glaccount[];

  @Input()
  selectedAccount: Glaccount;

  @Input()
  name: string;

  @Input()
  model: number;

  constructor(private accountsService: AccountsService, private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.accountsService.readAccount().subscribe(resp => {
      this.accounts = resp._embedded.glaccount;
      this.selectedAccount = (this.selectedAccount && this.selectedAccount.id) ? this.accounts.filter(one => one.id === this.selectedAccount.id)[0] : null;
      //this.selectedAccount = this.accounts[0];
      this.changeDetectorRef.detectChanges();
    })
  }

  public getSelectedAccountWithAmount(): CreateAccountAmount {
    return new CreateAccountAmount(this.selectedAccount, this.model)
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('CHANGES ' + changes['selectedAccount'].currentValue);
  }

  deleteClicked() {
    this.selectedAccount = null;
    this.model = 0;
  }
}
