import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlAccountSelectComponent } from './gl-account-select.component';

describe('GlAccountSelectComponent', () => {
  let component: GlAccountSelectComponent;
  let fixture: ComponentFixture<GlAccountSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlAccountSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlAccountSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
