export interface Self {
  href: string;
}

export interface GlAccount {
  href: string;
}

export interface Links {
  self: Self;
  glAccount: GlAccount;
}

export interface Glaccount {
  id: number;
  acctNo: string;
  description: string;
  glAccountType: string;
  _links: Links;
}

export interface Embedded {
  glaccount: Glaccount[];
}

export interface Self2 {
  href: string;
  templated: boolean;
}

export interface Profile {
  href: string;
}

export interface Links2 {
  self: Self2;
  profile: Profile;
}

export interface Page {
  size: number;
  totalElements: number;
  totalPages: number;
  number: number;
}

export interface GlAccountsResponse {
  _embedded: Embedded;
  _links: Links2;
  page: Page;
}

export class CreateAccountAmount {


  constructor(account: Glaccount, amount: number) {
    this.account = account;
    this.amount = amount;
  }

  account: Glaccount;
  amount: number;

}

export class GlCaseData {

}
