import { Injectable } from '@angular/core';
import {GlAccountsResponse} from "./gl-account-select/types";
import {Observable} from "rxjs/internal/Observable";
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {AccountCasesResponse, Content} from "./select-accounts-row/cases-types";

@Injectable()
export class AccountsService {

  constructor(private httpClient: HttpClient) { }

  public readAccount(): Observable<GlAccountsResponse> {
    return this.httpClient.get<GlAccountsResponse>(environment.base_url + '/glaccount');
  }


  public readAccountCases(): Observable<AccountCasesResponse> {
    return this.httpClient.get<AccountCasesResponse>(environment.base_url + '/mainbookcases');
  }

  public updateAccountCase(id: number, content: Content): Observable<Content> {
    return this.httpClient.put<Content>(environment.base_url + '/mainbookcases/' + id, content);
  }
}
