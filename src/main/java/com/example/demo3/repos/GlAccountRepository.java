package com.example.demo3.repos;

import com.example.demo3.entities.GlAccount;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

//@Repository
@RepositoryRestResource(collectionResourceRel = "glaccount", path = "glaccount")
public interface GlAccountRepository extends PagingAndSortingRepository<GlAccount, Long> {
}
