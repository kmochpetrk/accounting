package com.example.demo3.repos;

import com.example.demo3.entities.MainBookCase;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MainBookCaseRepository extends PagingAndSortingRepository<MainBookCase, Long> {
}
