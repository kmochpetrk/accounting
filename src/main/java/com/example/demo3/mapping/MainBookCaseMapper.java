package com.example.demo3.mapping;

import com.example.demo3.dtos.BankAccountTransDocDto;
import com.example.demo3.dtos.MainBookCaseDto;
import com.example.demo3.dtos.MainBookRowDto;
import com.example.demo3.entities.BankAccountTransDoc;
import com.example.demo3.entities.MainBookCase;
import com.example.demo3.entities.MainBookCaseType;
import com.example.demo3.entities.MainBookRow;
import org.springframework.util.Assert;

import java.util.HashSet;
import java.util.Set;

public class MainBookCaseMapper {


    public static MainBookCaseDto mapFromDb(MainBookCase mainBookCase) {
        if (mainBookCase instanceof BankAccountTransDoc) {
            BankAccountTransDoc bankAccountTransDoc = (BankAccountTransDoc)mainBookCase;
            BankAccountTransDocDto bankAccountTransDocDto = new BankAccountTransDocDto();
            bankAccountTransDocDto.setConstantSymbol(bankAccountTransDoc.getConstantSymbol());
            bankAccountTransDocDto.setVariableSymbol(bankAccountTransDoc.getVariableSymbol());
            bankAccountTransDocDto.setDescription(bankAccountTransDoc.getDescription());
            bankAccountTransDocDto.setId(bankAccountTransDoc.getId());
            bankAccountTransDocDto.setMainBookCaseType(bankAccountTransDoc.getMainBookCaseType());
            bankAccountTransDocDto.setMainBookRows(mapFromDbRows(bankAccountTransDoc.getMainBookRows()));
            return bankAccountTransDocDto;
        } else {
            throw new IllegalStateException();
        }

    }

    private static Set<MainBookRowDto> mapFromDbRows(Set<MainBookRow> mainBookRows) {
        Set<MainBookRowDto> returnSet = new HashSet<>();
        for (MainBookRow mainBookRow : mainBookRows) {
            final MainBookRowDto e = new MainBookRowDto();
            e.setAmount(mainBookRow.getAmount());
            e.setGlAccount(mainBookRow.getGlAccount());
            e.setId(mainBookRow.getId());
            e.setMbWriteType(mainBookRow.getMbWriteType());
            returnSet.add(e);
        }
        return returnSet;
    }

    public static MainBookCase mapToDb(MainBookCaseDto mainBookCaseDto) {
        BankAccountTransDoc mainBookCase = null;
        if (mainBookCaseDto.getMainBookCaseType() == MainBookCaseType.BANK_TRANSACTIONS_LIST) {
            mainBookCase = new BankAccountTransDoc();
            ((BankAccountTransDoc) mainBookCase).setVariableSymbol(mainBookCaseDto.getVariableSymbol());
            ((BankAccountTransDoc) mainBookCase).setConstantSymbol(mainBookCaseDto.getConstantSymbol());
        }
        mainBookCase.setDescription(mainBookCaseDto.getDescription());
        mainBookCase.setMainBookCaseType(mainBookCaseDto.getMainBookCaseType());
        mainBookCase.setId(mainBookCaseDto.getId());
        mainBookCase.setMainBookRows(mapRowsToDb(mainBookCaseDto.getMainBookRows(), mainBookCase));
        return mainBookCase;
    }

    private static Set<MainBookRow> mapRowsToDb(Set<MainBookRowDto> mainBookRows, MainBookCase mainBookCase) {
        Set<MainBookRow> mainBookRowSetReturn = new HashSet<>();
        for (MainBookRowDto mainBookRowDto : mainBookRows) {
            MainBookRow mainBookRow = new MainBookRow();
            mainBookRow.setMainBookCase(mainBookCase);
            mainBookRow.setMbWriteType(mainBookRowDto.getMbWriteType());
            mainBookRow.setGlAccount(mainBookRowDto.getGlAccount());
            mainBookRow.setAmount(mainBookRowDto.getAmount());
            mainBookRow.setId(mainBookRowDto.getId());
            mainBookRowSetReturn.add(mainBookRow);
        }
        return mainBookRowSetReturn;
    }
}
