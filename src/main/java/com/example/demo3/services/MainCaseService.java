package com.example.demo3.services;

import com.example.demo3.entities.MainBookCase;
import com.example.demo3.entities.MainBookRow;
import com.example.demo3.repos.MainBookCaseRepository;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import javax.transaction.Transactional;
import java.util.Set;

@Service
@Transactional
public class MainCaseService {

    @Autowired
    private MainBookCaseRepository mainBookCaseRepository;

    public Page<MainBookCase> getMainBookCases(Pageable pageable) {
        Page<MainBookCase> all = mainBookCaseRepository.findAll(pageable);
//        all.stream().forEach(mainBookCase -> {
//            mainBookCase.getMainBookRows().stream().forEach(mainBookRow -> {
//                //mainBookRow.setMainBookCase(null);
//            });
//        });
        return all;
    }

    public MainBookCase saveMainBookCase(MainBookCase mainBookCase) {
        final MainBookCase save = mainBookCaseRepository.save(mainBookCase);
        return save;
    }

}
