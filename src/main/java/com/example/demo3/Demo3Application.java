package com.example.demo3;

import com.example.demo3.entities.*;
import com.example.demo3.repos.GlAccountRepository;
import com.example.demo3.repos.MainBookCaseRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class Demo3Application {

    @Autowired
    private GlAccountRepository glAccountRepository;

    @Autowired
    private MainBookCaseRepository mainBookCaseRepository;

    public static void main(String[] args) {
        SpringApplication.run(Demo3Application.class, args);
    }

    @PostConstruct
    @Transactional
    public void postConstruct() {
        final GlAccount glAccountVat = new GlAccount();
        glAccountVat.setAcctNo("343100");
        glAccountVat.setDescription("VAT account");
        glAccountVat.setGlAccountType(GlAccountType.BC);
        GlAccount savedVat = glAccountRepository.save(glAccountVat);


        final GlAccount glAccount = new GlAccount();
        glAccount.setAcctNo("221100");
        glAccount.setDescription("Bank account");
        glAccount.setGlAccountType(GlAccountType.BC);
        GlAccount savedBankAccount = glAccountRepository.save(glAccount);
        //LoggerFactory.getLogger(Demo3Application.class).info("Saved " + save.getId());

        final GlAccount glAccount2 = new GlAccount();
        glAccount2.setAcctNo("601100");
        glAccount2.setDescription("Gain");
        glAccount2.setGlAccountType(GlAccountType.PLC);
        GlAccount savedGain = glAccountRepository.save(glAccount2);
        //LoggerFactory.getLogger(Demo3Application.class).info("Saved " + save.getId());
        BankAccountTransDoc bankAccountTransDoc = new BankAccountTransDoc();
        bankAccountTransDoc.setDescription("Income money to bank account for goods");
        bankAccountTransDoc.setMainBookCaseType(MainBookCaseType.BANK_TRANSACTIONS_LIST);
        bankAccountTransDoc.setVariableSymbol("1111");
        Set<MainBookRow> mainBookRows = new HashSet<>();
        MainBookRow mainBookRow = new MainBookRow();
        mainBookRow.setAmount(BigDecimal.valueOf(1200.50));
        mainBookRow.setGlAccount(savedBankAccount);
        mainBookRow.setMbWriteType(MbWriteType.DEBIT);
        mainBookRow.setMainBookCase(bankAccountTransDoc);
        mainBookRows.add(mainBookRow);

        MainBookRow mainBookRow2 = new MainBookRow();
        mainBookRow2.setAmount(BigDecimal.valueOf(1000.50));
        mainBookRow2.setGlAccount(savedGain);
        mainBookRow2.setMbWriteType(MbWriteType.CREDIT);
        mainBookRow2.setMainBookCase(bankAccountTransDoc);
        mainBookRows.add(mainBookRow2);

        MainBookRow mainBookRow3 = new MainBookRow();
        mainBookRow3.setAmount(BigDecimal.valueOf(200));
        mainBookRow3.setGlAccount(savedVat);
        mainBookRow3.setMbWriteType(MbWriteType.CREDIT);
        mainBookRow3.setMainBookCase(bankAccountTransDoc);
        mainBookRows.add(mainBookRow3);

        MainBookRow mainBookRow4 = new MainBookRow();
        mainBookRow4.setAmount(BigDecimal.valueOf(11500.50));
        mainBookRow4.setGlAccount(savedGain);
        mainBookRow4.setMbWriteType(MbWriteType.CREDIT);
        mainBookRow4.setMainBookCase(bankAccountTransDoc);
        //mainBookRows.add(mainBookRow4);

        bankAccountTransDoc.setMainBookRows(mainBookRows);
        mainBookCaseRepository.save(bankAccountTransDoc);
    }


//    @Bean
//    public WebMvcConfigurationSupport corsConfigurer() {
//        return new WebMvcConfigurationSupport() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry.addMapping("/**").allowedOrigins("*");
//            }
//        };
//    }

}
