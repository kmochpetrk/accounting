package com.example.demo3.dtos;

import com.example.demo3.entities.GlAccount;
import com.example.demo3.entities.MbWriteType;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
public class MainBookRowDto {
    private Long id;
    private BigDecimal amount;
    private MbWriteType mbWriteType;
    private GlAccount glAccount;
}
