package com.example.demo3.dtos;

import com.example.demo3.entities.MainBookCaseType;
import lombok.Data;

import java.util.Set;

@Data
public class MainBookCaseDto {
    private Long id;
    private MainBookCaseType mainBookCaseType;
    private String description;
    private Set<MainBookRowDto> mainBookRows;

    private String variableSymbol;
    private String constantSymbol;
}
