package com.example.demo3.dtos;

import lombok.Data;

@Data
public class BankAccountTransDocDto extends MainBookCaseDto {
    private String variableSymbol;
    private String constantSymbol;
}
