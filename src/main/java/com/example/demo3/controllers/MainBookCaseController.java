package com.example.demo3.controllers;

import com.example.demo3.dtos.MainBookCaseDto;
import com.example.demo3.entities.MainBookCase;
import com.example.demo3.mapping.MainBookCaseMapper;
import com.example.demo3.services.MainCaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MainBookCaseController {

    @Autowired
    private MainCaseService mainCaseService;

    @GetMapping(value = "/mainbookcases")
    public ResponseEntity<Page<MainBookCaseDto>> getMainBookCases(Pageable pageable) {
        Page<MainBookCase> all = mainCaseService.getMainBookCases(pageable);
        final Page<MainBookCaseDto> map = all.map(MainBookCaseMapper::mapFromDb);
        return ResponseEntity.ok(map);
    }

    @PutMapping(value = "/mainbookcases/{id}")
    public ResponseEntity<MainBookCaseDto> updateManBookCase(@RequestBody MainBookCaseDto mainBookCaseDto, @PathVariable Long id) {
        MainBookCase mainBookCase =  MainBookCaseMapper.mapToDb(mainBookCaseDto);
        final MainBookCase mainBookCase1 = mainCaseService.saveMainBookCase(mainBookCase);
        final MainBookCaseDto mainBookCaseDto1 = MainBookCaseMapper.mapFromDb(mainBookCase1);
        return ResponseEntity.ok(mainBookCaseDto1);
    }

}
