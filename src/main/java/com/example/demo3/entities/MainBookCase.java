package com.example.demo3.entities;

import lombok.Data;
import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "main_book_case")
@Data
@Inheritance(strategy=InheritanceType.JOINED)
public class MainBookCase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private MainBookCaseType mainBookCaseType;

    private String description;

    @OneToMany(mappedBy = "mainBookCase", cascade = CascadeType.ALL)
    private Set<MainBookRow> mainBookRows;

}
