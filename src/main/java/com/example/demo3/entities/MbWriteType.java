package com.example.demo3.entities;

public enum MbWriteType {
    DEBIT,
    CREDIT
}
