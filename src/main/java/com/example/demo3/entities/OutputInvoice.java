package com.example.demo3.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="output_invoice")
@PrimaryKeyJoinColumn(name="id")
@Data
public class OutputInvoice extends MainBookCase {
    private String customer;

}
