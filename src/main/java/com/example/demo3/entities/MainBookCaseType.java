package com.example.demo3.entities;

public enum MainBookCaseType {
    INPUT_INVOICE,
    OUTPUT_INVOICE,
    INPUT_RECEIPT,
    OUTPUT_RECEIPT,
    BANK_TRANSACTIONS_LIST,
    GOODS_INPUT_DOC,
    GOODS_OUTPUT_DOC
}
