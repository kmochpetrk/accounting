package com.example.demo3.entities;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Data;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="gl_account")
@Data
public class GlAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique = true)
    private String acctNo;

    @NotNull
    private String description;

    @Enumerated(EnumType.STRING)
    @NotNull
    private GlAccountType glAccountType;

    @JsonGetter
    public Long getId() {
        return id;
    }
}
