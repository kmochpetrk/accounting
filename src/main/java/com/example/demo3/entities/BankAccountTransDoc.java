package com.example.demo3.entities;

import com.example.demo3.entities.MainBookCase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="bank_account_trans_doc")
@PrimaryKeyJoinColumn(name="id")
@Data
public class BankAccountTransDoc extends MainBookCase {
    private String variableSymbol;
    private String constantSymbol;
}
