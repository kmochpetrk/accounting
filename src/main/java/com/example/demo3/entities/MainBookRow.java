package com.example.demo3.entities;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "main_book_row")
@Data
public class MainBookRow {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    private MbWriteType mbWriteType;

    @ManyToOne
    @JoinColumn(name = "gl_account")
    private GlAccount glAccount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "main_book_case")
    private MainBookCase mainBookCase;

}
